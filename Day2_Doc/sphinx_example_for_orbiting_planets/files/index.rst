.. Orbiting Planets documentation master file, created by
   sphinx-quickstart on Wed Sep 28 10:50:40 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Orbiting Planets's documentation!
============================================
This package can generate random planet coordinates in 2D that orbit the sun at the origin.

Usefull Video
==================
.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/hgI0p1zf31k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   module


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
