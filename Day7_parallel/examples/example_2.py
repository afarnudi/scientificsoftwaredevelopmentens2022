#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 13 19:39:50 2022

@author: ali
"""
import multiprocessing
import time
import matplotlib.pyplot as plt
import numpy as np

def pi_mc(seed):
    num_trials = 500_000
    counter = 0
    np.random.seed(seed)
    
    for j in range(num_trials):
        x_val = np.random.random_sample()
        y_val = np.random.random_sample()

        radius = x_val**2 + y_val**2

        if radius < 1:
            counter += 1
            
    return 4*counter/num_trials


def plot_pi_mc_concept():
    fig, ax = plt.subplots(nrows=1,ncols=1, figsize=(5,5))
    x = np.linspace(0,1,100)
    plt.fill_between(x, np.sqrt(1-x**2),0,alpha=0.1)
    plt.xlim(0,1.03);plt.ylim(0,1.03);plt.xlabel('X');plt.ylabel('Y');
    
    x = np.random.random(size=100)
    y = np.random.random(size=100)
    
    plt.plot(x,y,marker='.',linestyle='None');

def main(number_of_samples):
    start = time.perf_counter()
    
    # plot_pi_mc_concept()
    
    for seed in range(number_of_samples):
        pi_mc(seed)
    

    finish = time.perf_counter()
    print('sequential:')
    print(f'Took {round((finish-start),2)} second(s).')
    print(f'{round((finish-start)/number_of_samples,2)} second(s) per sample.')

def main_parallel(number_of_samples):
    start = time.perf_counter()
    
    seed_array = list(range(number_of_samples))
    
    with multiprocessing.Pool(processes=2) as pool:
        result = pool.map(pi_mc, seed_array)
    

    finish = time.perf_counter()
    print('parallel:')
    print(f'Took {round((finish-start),2)} second(s).')
    # print(f'{round((finish-start)/number_of_samples,2)} second(s) per sample.')

def main_benchmark(number_of_samples):
    execution_times=[]
    max_processses = 16
    for i in range(1,max_processses):
        start = time.perf_counter()
        
        # plot_pi_mc_concept()
        
        
        seed_array = list(range(number_of_samples))
        
        with multiprocessing.Pool(processes=i) as pool:
            result = pool.map(pi_mc, seed_array)
        
    
        finish = time.perf_counter()
        execution_times.append(round((finish-start),2))
    # print('parallel:')
    # print(f'Took {round((finish-start),2)} second(s).')
    plt.plot(range(1,max_processses), execution_times,'o-')
    plt.xlabel('# of processes')
    plt.ylabel('execution time [seconds]')

if __name__=="__main__":
    number_of_samples = 8*4
    # number_of_samples = 8*8
    # main(number_of_samples)
    # main_parallel(number_of_samples)
    main_benchmark(number_of_samples)
    
    
    
    
    