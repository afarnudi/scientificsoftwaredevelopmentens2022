#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 13 19:39:50 2022

@author: ali
"""
import multiprocessing
import time



def do_something():
    print('Sleeping for 1 seconad...')
    time.sleep(1)
    print('Finshed sleep')

def main():
    start = time.perf_counter()
    processes=[]
    for i in range(8):
        p = multiprocessing.Process(target=do_something)
        p.start()
        processes.append(p)
    
    for p in processes:
        p.join()

    finish = time.perf_counter()
    print(f'finished in {round(finish-start,2)} second(s).')

if __name__=="__main__":
    main()