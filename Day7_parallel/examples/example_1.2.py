#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 13 19:39:50 2022

@author: ali
"""
import concurrent.futures
import time
import numpy as np
import random


def do_something(job_num):
    # print('Sleeping for 1 seconad...')
    time.sleep(job_num)
    return f'Finshed {job_num}'

def main():
    start = time.perf_counter()
    secs = np.arange(20)
    # print(secs.reshape((2,10)))
    with concurrent.futures.ProcessPoolExecutor()as executor:
        
        results = [executor.submit(do_something, _) for _ in secs]
        
        for i, f in enumerate(concurrent.futures.as_completed(results)):
            print(i+1,f.result())
    
    finish = time.perf_counter()
    print(f'finished in {round(finish-start,2)} second(s).')

# def main2():
#     n=3
#     secs = np.arange(10*n)#[::-1]
#     random.shuffle(secs)
#     print(secs.reshape((n,10)))
#     output=''
#     start = time.perf_counter()
#     with concurrent.futures.ProcessPoolExecutor()as executor:
        
#         results = [executor.submit(do_something, _) for _ in secs]
        
#         for i, f in enumerate(concurrent.futures.as_completed(results)):
#             output+=f.result()+'\n'
    
#     finish = time.perf_counter()
#     print(output)
#     print(f'finished in {round(finish-start,2)} second(s).')

# def main3():
    
    
#     n=3
#     secs = np.asarray([29,28,27,26,25,24,23,22,21,20,
#                        10,11,12,13,14,15,16,17,18,19,
#                         9, 8, 7 ,6, 5, 4, 3 ,2, 1, 0])
#     # random.shuffle(secs)
#     print(secs.reshape((n,10)))
#     output=''
#     start = time.perf_counter()
#     with concurrent.futures.ProcessPoolExecutor()as executor:
        
#         results = [executor.submit(do_something, _) for _ in secs]
        
#         for i, f in enumerate(concurrent.futures.as_completed(results)):
#            output+=f.result()+'\n'
    
#     finish = time.perf_counter()
#     print(output)
#     print(f'finished in {round(finish-start,2)} second(s).')


if __name__=="__main__":
    main()