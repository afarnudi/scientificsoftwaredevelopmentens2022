#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 13 19:39:50 2022

@author: ali
"""
import multiprocessing
import time
import matplotlib.pyplot as plt
import numpy as np



def sum_function(beg, end, skip):
    sum_0=0
    for i in range(beg,end,skip):
        sum_0+=i
    return sum_0

def main_sum_serial(end):
    start = time.perf_counter()
    
    sum_function(0, end, 1)
    
    finish = time.perf_counter()
    print('parallel:')
    print(f'Took {round((finish-start),2)} second(s).')



def main_sum_parallel(end):
    start = time.perf_counter()
    
    max_processses = 8
    
    sum_all=[]
    begs = [i for i in range(max_processses)]
    ends = [end for i in range(max_processses)]
    skips = [max_processses for i in range(max_processses)]
    with multiprocessing.Pool(processes=max_processses) as pool:
        result = pool.starmap(sum_function, zip(begs, ends, skips))
        sum_all.append(result)
    
    
    finish = time.perf_counter()
    print(np.sum(sum_all))
    print('parallel:')
    print(f'Took {round((finish-start),2)} second(s).')


# def main_sum_serial_fast(end):
#     start = time.perf_counter()
    
#     np.sum(np.arange(end))
    
#     finish = time.perf_counter()
#     print('parallel:')
#     print(f'Took {round((finish-start),2)} second(s).')

if __name__=="__main__":
    end =100_000_001
    main_sum_serial(end)
    main_sum_parallel(end)
    # main_sum_serial_fast(end)
    
    
    
    