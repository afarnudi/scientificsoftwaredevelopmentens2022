#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 13 19:39:50 2022

@author: ali
"""
import multiprocessing
import time
import matplotlib.pyplot as plt
import numpy as np


def function_reflect(n, a):
    for i in range(len(a)):
        a[i] = -a[i]

def function_multiply(n, a):
    for i in range(len(a)):
        a[i] = n*a[i]

def main():
    val = multiprocessing.Value('d', 3.14)
    arr = multiprocessing.Array('d', range(10))
    num = val.value


    p_mult = multiprocessing.Process(target=function_multiply, args=(num, arr))
    p_mult.start()
    
    
    p_ref = multiprocessing.Process(target=function_reflect, args=(num, arr))
    p_ref.start()
    
    p_mult.join()
    p_ref.join()
    

    print(num)
    print(arr[:])

if __name__ == '__main__':
    main()    
    
    
    
    